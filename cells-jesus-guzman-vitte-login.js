{
  const {
    html,
  } = Polymer;
  /**
    `<cells-jesus-guzman-vitte-login>` Description.

    Example:

    ```html
    <cells-jesus-guzman-vitte-login></cells-jesus-guzman-vitte-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-jesus-guzman-vitte-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsJesusGuzmanVitteLogin extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-jesus-guzman-vitte-login';
    }

    static get properties() {
      return {
        user: {
          type: String,
          value: 'user'
        },
        pass: {
          type: String,
          value: 'password'
        },
        uservalue: {
          type: String,
          value: ''
        },
        passvalue: {
          type: String,
          value: ''
        },
        avatar: {
          type: String,
          value: '../app/resources/images/avatar.png'
        },
        lock: {
          type: String,
          value: '../app/resources/images/lock.png'
        },
        checked: {
          type: String,
          value: '../app/resources/images/checked.png'
        },
        error: {
          type: String,
          value: '../app/resources/images/error.png'
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-jesus-guzman-vitte-login-styles cells-jesus-guzman-vitte-login-shared-styles"></style>
      <slot></slot>
      <cells-basic-login
          icon-checked="coronita:checkmark"
          icon-unchecked="coronita:close"
          icon-toggle="coronita:menu"
        ></cells-basic-login>
      `;
    }

    reset() {
      Polymer.dom(this.root).querySelector('cells-basic-login').reset();
    }
  }

  customElements.define(CellsJesusGuzmanVitteLogin.is, CellsJesusGuzmanVitteLogin);
}